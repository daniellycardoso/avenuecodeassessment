Bugs from subtasks, as empty title and due date incomplete are scenarios that must be automated after the error behavior are settled. Since automating then as they are presented would be unused in the future.
Automate MS services such as 'subtask API' and 'Task API', validate values in the database.
Regarding Title size requirements, since the values inserted are trunked before creating a new task, after the defect resolution, the automation will work properly.
The "Validate Headers" scenarios isn't passing since there is a bug, but the automation is validating the correct header.