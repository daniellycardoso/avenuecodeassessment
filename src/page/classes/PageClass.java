package page.classes;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import util.Utils;

public class PageClass {
	
	WebDriver driver;
	JavascriptExecutor je;
	Utils util;
	
	public PageClass(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);

	}
	
	@FindBy(xpath = "/html/body/div[1]/div[2]/center/a[1]")
	private WebElement loginButton;
	@FindBy(id = "user_email")
	private WebElement emailBox;
	@FindBy(id = "user_password")
	private WebElement pwdBox;
	@FindBy(xpath = "/html/body/div[1]/div[2]/div[2]/form/input")
	private WebElement loginButton2;
	@FindBy(xpath = "/html/body/div[1]/div[1]/div/div[2]/ul[2]/li[1]/a")
	private WebElement loggedUser;
	@FindBy(xpath = "/html/body/div[1]/h1")
	private WebElement listHeader;
	@FindBy(id = "my_task")
	private WebElement myTask;
	@FindBy(id = "new_task")
	private WebElement boxNewTask;
	@FindBy(xpath = "/html/body/div[1]/div[2]/div[1]/form/div[2]/span")
	private WebElement addTask;
	@FindBy(xpath = "/html/body/div[1]/div[2]/div[2]/div[2]/table/tbody/tr/td[5]/button")
	private WebElement removeTask;
	@FindBy(xpath = "/html/body/div[1]/div[2]/div[2]/div[2]/table/tbody/tr[1]/td[4]/button")
	private WebElement subTask;
	@FindBy(id = "edit_task")
	private WebElement editTask;
	@FindBy(id = "new_sub_task")
	private WebElement subTaskTitle;
	@FindBy(xpath = "/html/body/div[4]/div/div/div[1]/h3")
	private WebElement subTaskID;
	@FindBy(id = "dueDate")
	private WebElement dueDate;
	@FindBy(id = "add-subtask")
	private WebElement addSubTask;
	@FindBy(xpath = "/html/body/div[4]/div/div/div[2]/div[2]/table/tbody/tr[1]/td[3]/button")
	private WebElement removeSubTask;
	@FindBy(xpath = "/html/body/div[1]/div[2]/div[2]/div[2]/table/tbody/tr[1]/td[1]/input")
	private WebElement doneTask;
	@FindBy(xpath = "/html/body/div[4]/div/div/div[2]/div[2]/table/tbody/tr/td[1]/input")
	private WebElement doneSubTask;
	
	
	public void clickLogin() {
		loginButton.click();
	}
	
	public void FillLoginInfo(String emailInfo, String pwdInfo) {
		emailBox.sendKeys(emailInfo);
		pwdBox.sendKeys(pwdInfo);
	}
	
	public void clickLogin2() {
		loginButton2.click();
	}
	
	public String loggedUser() {
		String text = loggedUser.getText();
		return text.substring(9, text.length()-1);
	}
	public String ListHeader() {
		return listHeader.getText();
	}
	
	public boolean isMyTaskThere() {
		util = new Utils(driver);
		return util.isThere(myTask);
	}
	
	public boolean isMyNewTaskThere() {
		util = new Utils(driver);
		return util.isThere(boxNewTask);
	}
	
	public boolean isMyIDThere() {
		util = new Utils(driver);
		return util.isThere(subTaskTitle);
	}
	
	public void clickMyTask() {
		myTask.click();
	}
	
	public void enterMyTask() {
		boxNewTask.sendKeys(Keys.ENTER);
	}
	
	public void fillNewTask(String text) {
		boxNewTask.sendKeys(text);
	}
	
	public void clickAddTask() {
		addTask.click();
	}
	
	public void clickRemoveTask() {
		removeTask.click();
	}
	
	public void clickSubTask() {
		subTask.click();
	}
	
	public void fillNewSubTask(String text) {
		subTaskTitle.sendKeys(text);
	}
	
	public void fillNewSTDueDate(String text) {
		dueDate.clear();
		dueDate.sendKeys(text);
	}
	
	public void clickAddSubTask() {
		addSubTask.click();
	}
	
	public void clickRemoveSubTask() {
		removeSubTask.click();
	}
	public void DoneTask() {
		doneTask.click();
	}
	
	public void DoneSubTask() {
		doneSubTask.click();
	}	
	
}
