package test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import page.classes.PageClass;
import util.Utils;

public class TestSteps {

	String baseUrl;
	WebDriver driver;
	PageClass home;
	Utils util;
	SoftAssert sa;

	public static final String dir = "C:\\Screenshots\\";

	// Validate headers Bug #7475
	@Test(priority=1)
	public void scenarioWelcomeMessage() {
		util.setDir(dir + "scenarioWelcomeMessage");
		home.clickLogin();
		home.FillLoginInfo("danielly.cardoso@hotmail.com", "nosoupforyou");
		util.screenshot();
		home.clickLogin2();
		sa.assertTrue(home.isMyTaskThere());
		util.screenshot();

		if (home.isMyTaskThere()) {
			home.clickMyTask();
		}
		String logUser = home.loggedUser();
		String expected = "Hey " + logUser + ", this is your todo list for today:";
		String lh = home.ListHeader();
		System.out.println(lh);
		System.out.println(expected);
		sa.assertEquals(lh, expected);
		util.screenshot();
		sa.assertAll();

	}

	// New task with 3 characters Bug # 7474
	@Test(priority=2)
	public void sccenarioNewTask3() {
		util.setDir(dir + "sccenarioNewTask3");
		home.clickLogin();
		home.FillLoginInfo("danielly.cardoso@hotmail.com", "nosoupforyou");
		util.screenshot();
		home.clickLogin2();
		sa.assertTrue(home.isMyTaskThere());
		util.screenshot();

		home.clickMyTask();
		if (home.isMyNewTaskThere()) {
			home.fillNewTask("abc");
			home.enterMyTask();
			util.screenshot();
		}
		sa.assertAll();

	}

	// New task with 250 characters Bug #7489
	@Test(priority=3)
	public void scenarioNewTask250() {
		util.setDir(dir + "scenarioNewTask250");
		sa = new SoftAssert();
		home.clickLogin();
		home.FillLoginInfo("danielly.cardoso@hotmail.com", "nosoupforyou");
		util.screenshot();
		home.clickLogin2();

		sa.assertTrue(home.isMyTaskThere());
		util.screenshot();
		home.clickMyTask();

		if (home.isMyNewTaskThere()) {
			home.fillNewTask(
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent eu diam a dolor porttitor efficitur at vel tortor. Cras sed imperdiet lacus, sit amet laoreet diam. Nunc purus diam, posuere vitae purus eu, sodales ornare lectus. Nullam et vestibulum");
			// Thread.sleep(400);
			home.clickAddTask();
			util.screenshot();
		}
		sa.assertAll();

	}

	// Remove any task
	@Test(priority=8)
	public void scenarioRemoveTask() {
		util.setDir(dir + "scenarioRemoveTask");
		sa = new SoftAssert();
		home.clickLogin();
		home.FillLoginInfo("danielly.cardoso@hotmail.com", "nosoupforyou");
		util.screenshot();
		home.clickLogin2();

		sa.assertTrue(home.isMyTaskThere());
		util.screenshot();
		home.clickMyTask();
		util.screenshot();
		try {
			home.clickRemoveTask();
			util.screenshot();
		} catch (NoSuchElementException e) {
			sa.assertTrue(false);
		} catch (TimeoutException e) {
			sa.assertTrue(false);
		}
		sa.assertAll();
	}

	// Create new subtask
	@Test(priority=4)
	public void scenarioSubtask() {
		util.setDir(dir + "scenarioSubtask");
		sa = new SoftAssert();
		home.clickLogin();
		home.FillLoginInfo("danielly.cardoso@hotmail.com", "nosoupforyou");
		util.screenshot();
		home.clickLogin2();

		sa.assertTrue(home.isMyTaskThere());
		util.screenshot();
		home.clickMyTask();
		util.screenshot();
		home.clickSubTask();
		util.screenshot();

		if (home.isMyIDThere()) {
			home.fillNewSubTask("Quabity Assuance");
			home.fillNewSTDueDate("01/01/2020");
			home.clickAddSubTask();
			util.screenshot();
		}
		sa.assertAll();
	}

	// Remove any subtask
	@Test(priority=6)
	public void scenarioRemoveSubTask() {
		util.setDir(dir + "scenarioRemoveSubTask");
		sa = new SoftAssert();
		home.clickLogin();
		home.FillLoginInfo("danielly.cardoso@hotmail.com", "nosoupforyou");
		util.screenshot();
		home.clickLogin2();
		
		sa.assertTrue(home.isMyTaskThere());
		util.screenshot();
		home.clickMyTask();
		util.screenshot();
		home.clickSubTask();
		util.screenshot();

		if (home.isMyIDThere()) {
			try {
				home.clickRemoveSubTask();
				util.screenshot();
			} catch (NoSuchElementException e) {
				sa.assertTrue(false);
			} catch (TimeoutException e) {
				sa.assertTrue(false);
			}
		}
		sa.assertAll();
	}

	// Mark task as done
	@Test(priority=7)
	public void scenarioMarkTaskDone() {
		util.setDir(dir + "scenarioMarkTaskDone");
		sa = new SoftAssert();
		home.clickLogin();
		home.FillLoginInfo("danielly.cardoso@hotmail.com", "nosoupforyou");
		util.screenshot();
		home.clickLogin2();

		sa.assertTrue(home.isMyTaskThere());
		util.screenshot();
		home.clickMyTask();
		util.screenshot();

		if (home.isMyNewTaskThere()) {
			try {
				home.DoneTask();
				util.screenshot();
			} catch (NoSuchElementException e) {
				sa.assertTrue(false);
			} catch (TimeoutException e) {
				sa.assertTrue(false);
			}
		}
		sa.assertAll();
	}

	// Mark subtask as done
	@Test(priority=5)
	public void scenarioMarSubkTaskDone() {
		util.setDir(dir + "scenarioMarSubkTaskDone");
		sa = new SoftAssert();
		home.clickLogin();
		home.FillLoginInfo("danielly.cardoso@hotmail.com", "nosoupforyou");
		util.screenshot();
		home.clickLogin2();

		sa.assertTrue(home.isMyTaskThere());
		home.clickMyTask();
		util.screenshot();
		home.clickSubTask();
		util.screenshot();

		if (home.isMyIDThere()) {
			try {
				home.DoneSubTask();
				util.screenshot();
			} catch (NoSuchElementException e) {
				sa.assertTrue(false);
			} catch (TimeoutException e) {
				sa.assertTrue(false);
			}
		}
		sa.assertAll();
	}

	@BeforeMethod
	public void beforeMethod() {
		sa = new SoftAssert();
		System.setProperty("webdriver.gecko.driver", "C:\\geckodriver.exe");

		baseUrl = "https://qa-test.avenuecode.com";
		driver = new FirefoxDriver();
		home = new PageClass(driver);
		util = new Utils(driver);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		driver.get(baseUrl);
	}

	@AfterMethod
	public void afterMethod() {
		driver.close();
	}
}
